package com.xiaolei.openokjoke.Cache

import android.content.Context
import com.xiaolei.okhttputil.Catch.CacheImpl.FileCacheImpl
import com.xiaolei.okhttputil.Catch.Interfaces.CacheInterface
import java.io.File

/**
 * Created by xiaolei on 2018/3/13.
 */
object CacheUtil
{
    fun getCacheInstance(context: Context): CacheInterface
    {
        val dir = File(context.cacheDir, "mGlideLog")
        return FileCacheImpl.getInstance(dir, context)
    }
}