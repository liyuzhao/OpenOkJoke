package com.xiaolei.openokjoke.Activitys

import android.os.Bundle
import com.xiaolei.openokjoke.Base.BaseActivity
import com.xiaolei.openokjoke.R

/**
 * 测试广告的界面
 * Created by xiaolei on 2018/3/21.
 */
class TestADActivity : BaseActivity()
{
    override fun onCreate(savedInstanceState: Bundle?)
    {
        setContentView(R.layout.activity_test_ad)
        super.onCreate(savedInstanceState)
    }

    override fun initObj()
    {

    }

    override fun initView()
    {

    }

    override fun initData()
    {

    }

    override fun setListener()
    {

    }

    override fun loadData()
    {
        
    }
}