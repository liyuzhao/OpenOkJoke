package com.xiaolei.openokjoke.Base

import android.graphics.Color
import android.view.View
import com.githang.statusbar.StatusBarCompat
import com.xiaolei.easyfreamwork.base.BaseV4Activity
import com.xiaolei.openokjoke.BuildConfig
import com.umeng.analytics.MobclickAgent



/**
 * Created by xiaolei on 2017/12/6.
 */
abstract class BaseV4Activity : BaseV4Activity()
{
    override fun onSetContentView()
    {
        setStatusBar(false)
    }

    fun setStatusBar(lightStatusBar: Boolean)
    {
        val decorView = window.decorView
        val option = View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN or View.SYSTEM_UI_FLAG_LAYOUT_STABLE
        decorView.systemUiVisibility = option
        window.statusBarColor = Color.TRANSPARENT
        StatusBarCompat.setStatusBarColor(this, Color.TRANSPARENT, lightStatusBar)
    }


    override abstract fun initObj()
    override abstract fun initView()
    override abstract fun initData()
    override abstract fun setListener()
    override abstract fun loadData()

    /**
     * 只有在DEBUG模式下，才会弹出
     *
     * @param object
     */
    fun AlertDebug(`object`: Any)
    {
        if (BuildConfig.DEBUG)
        {
            Alert(`object`)
        }
    }

    public override fun onResume()
    {
        super.onResume()
        MobclickAgent.onResume(this)
    }

    public override fun onPause()
    {
        super.onPause()
        MobclickAgent.onPause(this)
    }
}